# Taula de continguts
- [Com interactuar amb IRC des de Matrix][matrix-irc] 13/12/2020
- [Què és Salsa?][que-es-salsa] 21/12/2020
- [Què és Debian Social][que-es-d-social] 28/12/2020
- [Què és Debian Academy][que-es-d-academy] 17/01/2021

[matrix-irc]: content/com-interactuar-amb-irc-des-de-matrix.md
[que-es-salsa]: content/que-es-salsa.md
[que-es-d-social]: content/que-es-debian-social.md
[que-es-d-academy]: content/que-es-debian-academy.md

# Descripció
Aquest repositori conté diferents articles o píndoles creats per i per a la
comunitat de [DebianCat][debiancat], la comunitat d'usuaris de Debian de parla catalana.

Els documents estan en format Markdown i per col·laborar-hi només cal:
1. Crear el contingut al directori `content/`
2. Afegir un enllaç a aquest fitxer amb una descripció, si vols.
3. Crear una merge request

Si tens dubtes, posa't en contacte a través dels canals habituals.

[debiancat]: https://wiki.debian.org/LocalGroups/DebianCat
